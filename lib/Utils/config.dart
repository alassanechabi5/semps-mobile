import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Configuration {
  static String base_url = "192.168.0.104:3000";

  static const vertFonceColor = Color(0xff088c2e);
  static const vertLightColor = Color(0xFF14a01b);
  static const orangeFonceColor = Color(0xffd5ae0f);
  static const rougeFonceColor = Color(0xFFc5350e);
  static const bleuFonceColor = Color(0xff158be0);
  static const grisColor = Color(0xFF5E6465);
}

//Alert dialog
void displayDialog(
    BuildContext dialogCtx, String titre, String msg, String type) {
  if (type == "success") {
    showDialog(
      context: dialogCtx,
      builder: (BuildContext ctx) {
        return AlertDialog(
          title: Text(titre),
          content: Text(msg),
          actions: [
            /*TextButton(
              onPressed: () => Navigator.pop(ctx, 'Fermer'),
              child: const Text('Fermer'),
            ),*/
            TextButton(
              onPressed: () => Navigator.pop(ctx),
              child: const Text('OK'),
              style: ButtonStyle(
                  elevation: MaterialStateProperty.all(15),
                  foregroundColor: MaterialStateProperty.all(Colors.white),
                  backgroundColor: MaterialStateProperty.all(Configuration.vertFonceColor),
                  shadowColor: MaterialStateProperty.all(Configuration.vertFonceColor),
                  padding: MaterialStateProperty.all(const EdgeInsets.all(5)),
                  fixedSize: MaterialStateProperty.all(const Size(100, 40))),
            ),
          ],
          actionsAlignment: MainAxisAlignment.center,
          icon: Image.asset(
            'assets/success.jpeg',
            width: 80,
            height: 80,
          ),
        );
      },
    );
  }
  else if (type == "warning") {
    showDialog(
      context: dialogCtx,
      builder: (BuildContext ctx) {
        return AlertDialog(
          title: Text(titre),
          content: Text(msg),
          actions: [
            /*TextButton(
              onPressed: () => Navigator.pop(ctx, 'Fermer'),
              child: const Text('Fermer'),
            ),*/
            TextButton(
              onPressed: () => Navigator.pop(ctx, 'OK'),
              child: const Text('OK'),
              style: ButtonStyle(
                  elevation: MaterialStateProperty.all(15),
                  foregroundColor: MaterialStateProperty.all(Colors.white),
                  backgroundColor: MaterialStateProperty.all(Configuration.orangeFonceColor),
                  shadowColor: MaterialStateProperty.all(Configuration.orangeFonceColor),
                  padding: MaterialStateProperty.all(const EdgeInsets.all(5)),
                  fixedSize: MaterialStateProperty.all(const Size(100, 40))),
            ),
          ],
          actionsAlignment: MainAxisAlignment.center,
          icon: Image.asset(
            'assets/warning.jpeg',
            width: 80,
            height: 80,
          ),
        );
      },
    );
  }
}
//