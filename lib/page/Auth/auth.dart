import 'package:flutter/material.dart';
import 'package:stems_app/page/home_screen.dart';

import '../../Utils/config.dart';
import 'package:http/http.dart' as http;

class auth extends StatefulWidget {
  @override
  _MyappState createState() => _MyappState();
}

class _MyappState extends State<auth> {

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        height: height,
        width: width,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: width,
                height: height*0.45,
                child: Image.asset('assets/yoga.png',fit: BoxFit.fill,),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text('Connexion',style: TextStyle(fontSize: 25.0,fontWeight: FontWeight.bold),),
                  ],
                ),
              ),
              SizedBox(height: 30.0,),
              TextField(

                decoration: InputDecoration(
                  hintText: 'Email',
                  suffixIcon: Icon(Icons.email),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
              ),
              SizedBox(height: 20.0,),
              TextField(
                obscureText: true,
                decoration: InputDecoration(
                  hintText: 'Mot de passe',
                  suffixIcon: Icon(Icons.visibility_off),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
              ),
              SizedBox(height: 30.0,),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Mot de passe oublié?',style: TextStyle(fontSize: 12.0),),
                    ElevatedButton(
                      child: Text('Se connecter'),
                      //color: Color(0xffEE7B23),
                      onPressed: (){
                        Navigator.push( context, MaterialPageRoute(builder: (context) => HomeScreen()));

                      },
                    ),
                  ],
                ),
              ),
              SizedBox(height:20.0),
              GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>Second()));
                },
                child: Text.rich(
                  TextSpan(
                      text: 'Vous n\'avez pas de compte ?',
                      children: [
                        TextSpan(
                          text: 'S\'inscrire',
                          style: TextStyle(
                              color: Color(0xffEE7B23)
                          ),
                        ),
                      ]
                  ),
                ),
              ),


            ],
          ),
        ),
      ),
    );
  }
}

class Second extends StatefulWidget {
  @override
  _SecondState createState() => _SecondState();
}

class _SecondState extends State<Second> {

  final _formKey = GlobalKey<FormState>();

  TextEditingController nm=TextEditingController();
  TextEditingController pnm=TextEditingController();
  TextEditingController usernm=TextEditingController();
  TextEditingController email=TextEditingController();
  TextEditingController mdp=TextEditingController();
  TextEditingController cmdp=TextEditingController();

  //color

  //
  //Validator
  String? validateNom(String value) {
    if (value == null || value.isEmpty) {
      return 'Veuillez entrer votre nom';
    }else if (value.length < 3)
      return 'Le nom doit contenir au moins 3 caractères';
    else
      return null;
  }

  String? validatePrenom(String value) {
    if (value == null || value.isEmpty) {
      return 'Veuillez entrer votre prenom';
    }else if (value.length < 3)
      return 'Le prénom doit contenir au moins 3 caractère';
    else
      return null;
  }

  String? validateUsername(String value) {
    if (value == null || value.isEmpty) {
      return 'Veuillez entrer votre Pseudo';
    }else if (value.length < 3)
      return 'Le prénom doit contenir au moins 3 caractère';
    else
      return null;
  }

  String? validatePassword(String value) {
    if (value == null || value.isEmpty) {
      return 'Veuillez entrer votre mot de passe';
    }else if (value.length < 8)
      return 'Le mot de passe doit contenir au moins 8 caractères';
    else
      return null;
  }
  String? validatecPassword(String value) {
    if (value != mdp.text)
      return 'Les mots de passe ne sont pas identiques';
    else
      return null;
  }


  String? validatorEmail (value) {
  if (value == null || value.isEmpty) {
  return 'Veuillez entrer votre adresse e-mail';
  } else if (!value.contains('@')) {
  return 'Veuillez entrer une adresse e-mail valide';
  }
  return null;
  }
  //Fin de validation


  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        height: height,
        width: width,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            //key: _formKey,
            children: [
              Container(
                width: width,
                height: height*0.4,
                child: Image.asset('assets/play.png',fit: BoxFit.fill,),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text('S\'inscrire',style: TextStyle(fontSize: 25.0,fontWeight: FontWeight.bold),),
                  ],
                ),
              ),
              Form(
                  key: _formKey,
                  child: Column(
                children: [
                  new TextFormField(
                    obscureText: false,
                    validator: (value) => value!.isEmpty?"Champ incorrect":null,
                    controller: nm,
                    decoration: InputDecoration(
                      hintText: 'Nom',
                      suffixIcon: Icon(Icons.person),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),

                  ),
                  SizedBox(height: 20.0,),
                  new TextFormField(
                    obscureText: false,
                    validator: (value) => value!.isEmpty?"Champ incorrect":null,
                    controller: pnm,
                    decoration: InputDecoration(
                      hintText: 'Prénom',
                      suffixIcon: Icon(Icons.person),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                  ),
                  SizedBox(height: 20.0,),
                  new TextFormField(
                    obscureText: false,
                    validator: (value) => value!.isEmpty?"Champ incorrect":null,
                    controller: usernm,
                    decoration: InputDecoration(
                      hintText: 'Pseudo',
                      suffixIcon: Icon(Icons.person),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                  ),
                  SizedBox(height: 20.0,),
                  new TextFormField(
                    controller: email,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Veuillez entrer votre adresse e-mail';
                      } else if (!value.contains('@')) {
                        return 'Veuillez entrer une adresse e-mail valide';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      hintText: 'Email',
                      suffixIcon: Icon(Icons.email),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                  ),
                  SizedBox(height: 20.0,),
                  new TextFormField(
                    obscureText: true,
                    validator: (value) {
                      if (value == null || value.isEmpty || value.length<8) {
                        return 'Mot de passe incomplet (min. 8 caractères requis)';
                      }
                      return null;
                    },
                    controller: mdp,
                    decoration: InputDecoration(
                      hintText: 'Mot de passe',
                      suffixIcon: Icon(Icons.visibility_off),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                  ),
                  SizedBox(height: 20.0,),
                  new TextFormField(
                    obscureText: true,
                    validator: (value){
                      validatecPassword(value!);
                    },
                    controller: cmdp,
                    decoration: InputDecoration(
                      hintText: 'Confirmer mot de passe',
                      suffixIcon: Icon(Icons.visibility_off),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                  ),
                  // SizedBox(height: 30.0,),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        // Text('Forget password?',style: TextStyle(fontSize: 12.0),),
                        ElevatedButton(
                          child: Text('S\'inscrire'),
                          // color: Color(0xffEE7B23),
                          onPressed: (){
                            if(_formKey.currentState!.validate()){
                             getData(nm.text, pnm.text, usernm.text, email.text, mdp.text);
                            }else {
                              displayDialog(context,
                                  "Erreur de validation",
                                  "Veuillez remplir tous les champs",
                                  "warning");
                            }
                           // Navigator.push(context, MaterialPageRoute(builder: (context)=>HomeScreen()));

                          },
                        ),
                      ],
                    ),
                  ),
                ],
              )),
             // SizedBox(height: 20.0,),

             // SizedBox(height:20.0),
              GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>auth()));
                },
                child: Text.rich(
                  TextSpan(
                      text: 'Vous avez déjà de compte ?',
                      children: [
                        TextSpan(
                          text: 'Se connecter',
                          style: TextStyle(
                              color: Color(0xffEE7B23)
                          ),
                        ),
                      ]
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }


  Future getData(String nom , String prenom, String username , String email, String mdp) async {
    var url = Uri.http('${Configuration.base_url}', 'users');

    var response = await http.post(url, body:
    {
      "firstname": nom,
      "lastname": prenom,
      "username": username,
      "email": email,
      "password": mdp,
    }
    );
    if (response.statusCode == 201) {
      displayDialog(context,
          "Enregistrement",
          "Votre compte a été créé avec succès! "
              "\nVeuillez vous connecter",
          "success");
    } else {
      displayDialog(context,
          "Erreur de validation",
          "${response.body}",
          "warning");
    }
  }
}

